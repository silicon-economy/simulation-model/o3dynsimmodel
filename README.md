# O3dyn Simulation Model

![O3dyn Model](documentation/images/o3dyn_overview.png)

O3dyn from the Fraunhofer Institute for Material Flow and Logistics (IML) is a highly dynamic and autonomous mobile robot. It can transport large loads in pallet format omnidirectionally with a travel speed of up to 36 km/h - both indoor and outdoor. The O3dyn Simulation Model tries to closely match the real vehicle and offers similar dynamics and sensor data. This simulation approach can reduce development times. First, prototypes can be tested in digital reality before they are built. Second, hardware and software development can be decoupled in this way. Details about O3dyn itself, the simulation model, usage, and use cases can be found in the [documentation](/documentation/index.adoc).

The actually provided O3dyn version is not as dynamic as the real robot. The air suspension system is much stiffer in simulation resulting in less tilting behavior. There is a new version which replicates the behavior better and reduces the sim2real gap. An updated will be provided soon.

## Setup 
The requirements and installation of the O3dyn simulation model are described in [chapter 2](/documentation/02_requirements.adoc) and [chapter 3](/documentation/03_installation.adoc) of the documentation.
## Usage
The usage of the O3dyn model by controlling it manually, via Python scripts or with ROS is described in [chapter 4](/documentation/04_usage.adoc). In addition, different use cases are described in [chapter 5](/documentation/05_use_cases.adoc).

## Resources
- IEEE ICRA 2024: [Simulation Modeling of Highly Dynamic Omnidirectional Mobile Robots Based on Real-World Data](https://ieeexplore.ieee.org/document/10611459), M. Wiedemann, O. Ahmed, A. Dieckhöfer, R. Gasoto and S. Kerner, 2024 IEEE International Conference on Robotics and Automation (ICRA), Yokohama, Japan, 2024, pp. 16923-16929, doi: 10.1109/ICRA57147.2024.10611459.
- ROSCon 2023:  [Simulation of Highly Dynamic Omnidirectional Robots in Isaac Sim](https://vimeo.com/879001799/0e54e12495)
- GTC Spring 2023: [Simulation-Based Development of Mobile Robots: Technical Hands-On to the Next-Generation Logistics Robot O³dyn](https://www.nvidia.com/en-us/on-demand/session/gtcspring23-s51911/)
- GTC Spring 2022: [Towards a Digital Reality in Logistics Automation: Optimization of Sim-to-Real](https://www.nvidia.com/en-us/on-demand/session/gtcspring22-s42559/)


## License
Licensed under the Open Logistics Foundation License 1.3.
For details on the licensing terms, see the LICENSE file.

## Contact information

Maintainer:
- Marvin Wiedemann <a href="mailto:marvin.wiedemann@iml.fraunhofer.de?">marvin.wiedemann@iml.fraunhofer.de</a>

Development Team:
- Anna Dieckhöfer <a href="mailto:anna.dieckhoefer@iml.fraunhofer.de?">anna.dieckhoefer@iml.fraunhofer.de</a>
- Renato Gasoto <a href="mailto:rgasoto@nvidia.com?">rgasoto@nvidia.com</a>
- Ece Erdim <a href="mailto:ece.erdim@iml.fraunhofer.de?">ece.erdim@iml.fraunhofer.de</a>
